<?php

namespace Udibagas\Rubarta\Models;

use October\Rain\Database\Model;
use System\Models\File;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'udibagas_rubarta_products';

    /**
     * @var array Validation rules
     */
    public $rules = [];

    public $attachOne = [
        'image' => File::class
    ];

    public $attachMany = [
        'galleries' => File::class
    ];
}
