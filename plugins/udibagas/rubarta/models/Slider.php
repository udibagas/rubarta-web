<?php

namespace Udibagas\Rubarta\Models;

use October\Rain\Database\Model;
use System\Models\File;

/**
 * Model
 */
class Slider extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'udibagas_rubarta_sliders';

    /**
     * @var array Validation rules
     */
    public $rules = [];

    public $attachOne = [
        'image' => File::class
    ];
}
