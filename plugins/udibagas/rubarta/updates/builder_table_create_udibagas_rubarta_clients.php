<?php namespace Udibagas\Rubarta\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUdibagasRubartaClients extends Migration
{
    public function up()
    {
        Schema::create('udibagas_rubarta_clients', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('udibagas_rubarta_clients');
    }
}
