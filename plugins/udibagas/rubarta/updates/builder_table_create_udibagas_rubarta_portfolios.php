<?php namespace Udibagas\Rubarta\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUdibagasRubartaPortfolios extends Migration
{
    public function up()
    {
        Schema::create('udibagas_rubarta_portfolios', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 255);
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('udibagas_rubarta_portfolios');
    }
}
