<?php namespace Udibagas\Rubarta\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUdibagasRubartaProducts extends Migration
{
    public function up()
    {
        Schema::create('udibagas_rubarta_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('name');
            $table->text('description');
            $table->text('body');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('udibagas_rubarta_products');
    }
}
