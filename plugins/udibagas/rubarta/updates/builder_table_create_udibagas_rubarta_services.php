<?php namespace Udibagas\Rubarta\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUdibagasRubartaServices extends Migration
{
    public function up()
    {
        Schema::create('udibagas_rubarta_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('title');
            $table->text('description');
            $table->string('icon');
            $table->text('body');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('udibagas_rubarta_services');
    }
}
