<?php namespace Udibagas\Rubarta\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUdibagasRubartaPortfolios extends Migration
{
    public function up()
    {
        Schema::table('udibagas_rubarta_portfolios', function($table)
        {
            $table->string('tag')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('udibagas_rubarta_portfolios', function($table)
        {
            $table->dropColumn('tag');
        });
    }
}
