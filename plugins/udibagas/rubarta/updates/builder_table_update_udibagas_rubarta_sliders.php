<?php namespace Udibagas\Rubarta\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUdibagasRubartaSliders extends Migration
{
    public function up()
    {
        Schema::table('udibagas_rubarta_sliders', function($table)
        {
            $table->string('name', 255)->change();
            $table->string('title', 255)->change();
            $table->string('button', 255)->change();
            $table->string('url', 255)->change();
        });
    }
    
    public function down()
    {
        Schema::table('udibagas_rubarta_sliders', function($table)
        {
            $table->string('name', 191)->change();
            $table->string('title', 191)->change();
            $table->string('button', 191)->change();
            $table->string('url', 191)->change();
        });
    }
}
