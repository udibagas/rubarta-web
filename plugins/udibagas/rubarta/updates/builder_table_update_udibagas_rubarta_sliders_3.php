<?php namespace Udibagas\Rubarta\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUdibagasRubartaSliders3 extends Migration
{
    public function up()
    {
        Schema::table('udibagas_rubarta_sliders', function($table)
        {
            $table->dropColumn('slug');
        });
    }
    
    public function down()
    {
        Schema::table('udibagas_rubarta_sliders', function($table)
        {
            $table->string('slug', 255)->nullable();
        });
    }
}
